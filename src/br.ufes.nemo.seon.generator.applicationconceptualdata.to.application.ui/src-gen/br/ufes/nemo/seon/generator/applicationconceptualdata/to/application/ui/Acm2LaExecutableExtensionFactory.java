/*
 * generated by Xtext 2.21.0
 */
package br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.ui;

import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.ui.internal.ApplicationActivator;
import com.google.inject.Injector;
import org.eclipse.core.runtime.Platform;
import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;

/**
 * This class was generated. Customizations should only happen in a newly
 * introduced subclass. 
 */
public class Acm2LaExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	protected Bundle getBundle() {
		return Platform.getBundle(ApplicationActivator.PLUGIN_ID);
	}
	
	@Override
	protected Injector getInjector() {
		ApplicationActivator activator = ApplicationActivator.getInstance();
		return activator != null ? activator.getInjector(ApplicationActivator.BR_UFES_NEMO_SEON_GENERATOR_APPLICATIONCONCEPTUALDATA_TO_APPLICATION_ACM2LA) : null;
	}

}
