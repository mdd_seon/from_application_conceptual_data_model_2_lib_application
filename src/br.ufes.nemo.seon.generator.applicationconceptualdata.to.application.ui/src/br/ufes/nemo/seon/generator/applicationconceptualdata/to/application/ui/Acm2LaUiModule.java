/*
 * generated by Xtext 2.21.0
 */
package br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * Use this class to register components to be used within the Eclipse IDE.
 */
public class Acm2LaUiModule extends AbstractAcm2LaUiModule {

	public Acm2LaUiModule(AbstractUIPlugin plugin) {
		super(plugin);
	}
}
