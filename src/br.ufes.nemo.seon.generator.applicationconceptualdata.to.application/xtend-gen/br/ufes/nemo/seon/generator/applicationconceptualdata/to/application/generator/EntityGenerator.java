package br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.generator;

import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.Attribute;
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.Configuration;
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.Description;
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.Entity;
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.Function;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.AbstractGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class EntityGenerator extends AbstractGenerator {
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  private String lib_name;
  
  private String PATH = "src/";
  
  private List<Entity> entities;
  
  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
    Iterable<Configuration> _filter = Iterables.<Configuration>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Configuration.class);
    for (final Configuration configuration : _filter) {
      this.lib_name = configuration.getLib().getName();
    }
    fsa.generateFile(((this.PATH + this.lib_name) + "/__init__.py"), "");
    Iterable<Entity> _filter_1 = Iterables.<Entity>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Entity.class);
    for (final Entity e : _filter_1) {
      String _lowerCase = this._iQualifiedNameProvider.getFullyQualifiedName(e).toString("/").toLowerCase();
      String _plus = (((this.PATH + this.lib_name) + "/") + _lowerCase);
      String _plus_1 = (_plus + ".py");
      fsa.generateFile(_plus_1, 
        this.compile(e));
    }
    ArrayList<Entity> _arrayList = new ArrayList<Entity>();
    this.entities = _arrayList;
    Iterables.<Entity>addAll(this.entities, Iterables.<Entity>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Entity.class));
    fsa.generateFile((((this.PATH + this.lib_name) + "/") + "factories.py"), this.createFactory(this.entities));
  }
  
  public CharSequence createFactory(final List<Entity> entities) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("import factory");
    _builder.newLine();
    {
      for(final Entity e : entities) {
        _builder.append("from .");
        QualifiedName _lowerCase = this._iQualifiedNameProvider.getFullyQualifiedName(e).toLowerCase();
        _builder.append(_lowerCase);
        _builder.append(" import ");
        QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(e);
        _builder.append(_fullyQualifiedName);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    {
      for(final Entity e_1 : entities) {
        _builder.append("class ");
        QualifiedName _fullyQualifiedName_1 = this._iQualifiedNameProvider.getFullyQualifiedName(e_1);
        _builder.append(_fullyQualifiedName_1);
        _builder.append("Factory(factory.Factory):");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("class Meta:");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("model = ");
        QualifiedName _fullyQualifiedName_2 = this._iQualifiedNameProvider.getFullyQualifiedName(e_1);
        _builder.append(_fullyQualifiedName_2, "\t\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t  ");
        _builder.newLine();
      }
    }
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence compile(final Entity e) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("import logging");
    _builder.newLine();
    _builder.append("logging.basicConfig(level=logging.INFO)");
    _builder.newLine();
    _builder.newLine();
    {
      Description _description = e.getDescription();
      boolean _tripleNotEquals = (_description != null);
      if (_tripleNotEquals) {
        {
          boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(e.getDescription().getTextfield());
          boolean _not = (!_isNullOrEmpty);
          if (_not) {
            _builder.append("\"\"\" ");
            String _textfield = e.getDescription().getTextfield();
            _builder.append(_textfield);
            _builder.append(" \"\"\"");
          }
        }
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("class ");
    String _name = e.getName();
    _builder.append(_name);
    {
      Entity _superType = e.getSuperType();
      boolean _tripleNotEquals_1 = (_superType != null);
      if (_tripleNotEquals_1) {
        _builder.append("(");
        QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(e.getSuperType());
        _builder.append(_fullyQualifiedName);
        _builder.append(")");
      } else {
        _builder.append("()");
      }
    }
    _builder.append(":");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("def __init__(self):");
    _builder.newLine();
    {
      Entity _superType_1 = e.getSuperType();
      boolean _tripleNotEquals_2 = (_superType_1 != null);
      if (_tripleNotEquals_2) {
        _builder.append("\t\t");
        _builder.append("super(");
        String _name_1 = e.getName();
        _builder.append(_name_1, "\t\t");
        _builder.append(",self).__init__()");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<Attribute> _attributes = e.getAttributes();
      for(final Attribute a : _attributes) {
        _builder.append("\t\t");
        CharSequence _compile = this.compile(a);
        _builder.append(_compile, "\t\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t");
    _builder.newLine();
    {
      EList<Function> _functions = e.getFunctions();
      for(final Function f : _functions) {
        _builder.append("\t");
        CharSequence _compile_1 = this.compile(f);
        _builder.append(_compile_1, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
  
  public CharSequence compile(final Attribute a) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    {
      Description _description = a.getDescription();
      boolean _tripleNotEquals = (_description != null);
      if (_tripleNotEquals) {
        {
          boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(a.getDescription().getTextfield());
          boolean _not = (!_isNullOrEmpty);
          if (_not) {
            _builder.append("\"\"\" ");
            String _textfield = a.getDescription().getTextfield();
            _builder.append(_textfield);
            _builder.append(" \"\"\"");
          }
        }
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("self.");
    String _name = a.getName();
    _builder.append(_name);
    _builder.append(" = None");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  public CharSequence compile(final Function o) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    {
      Description _description = o.getDescription();
      boolean _tripleNotEquals = (_description != null);
      if (_tripleNotEquals) {
        {
          boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(o.getDescription().getTextfield());
          boolean _not = (!_isNullOrEmpty);
          if (_not) {
            _builder.append("\"\"\" ");
            String _textfield = o.getDescription().getTextfield();
            _builder.append(_textfield);
            _builder.append(" \"\"\"");
          }
        }
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("def ");
    String _name = o.getName();
    _builder.append(_name);
    _builder.append("(self");
    {
      EList<String> _params = o.getParams();
      for(final String f : _params) {
        _builder.append(",");
        _builder.append(f);
      }
    }
    _builder.append("): ");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("try:");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("logging.info(\"Start function: ");
    String _name_1 = o.getName();
    _builder.append(_name_1, "\t\t");
    _builder.append("\")");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("#TO DO");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("raise Exception(\'TO DO\', \'TO DO\')\t\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("logging.info(\"End function: ");
    String _name_2 = o.getName();
    _builder.append(_name_2, "\t\t");
    _builder.append("\")");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("except Exception as e: ");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("logging.error(\"OS error: {0}\".format(e))");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("logging.error(e.__dict__) ");
    _builder.newLine();
    return _builder;
  }
}
