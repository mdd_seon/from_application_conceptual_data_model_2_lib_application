package br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.generator;

import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.Attribute;
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.Description;
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.Entity;
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.Function;
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.ManyToMany;
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.OneToMany;
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.OneToOne;
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.Relation;
import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.AbstractGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;

@SuppressWarnings("all")
public class DocumenationGenerator extends AbstractGenerator {
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  private List<Entity> entities;
  
  private String PATH = "src/docs/";
  
  private String GITLAB_PATH = "https://gitlab.com/mdd_seon/from_application_conceptual_data_model_2_lib_application";
  
  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
    ArrayList<Entity> _arrayList = new ArrayList<Entity>();
    this.entities = _arrayList;
    Iterables.<Entity>addAll(this.entities, Iterables.<Entity>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Entity.class));
    fsa.generateFile((this.PATH + "README.md"), this.createDocumentation(this.entities));
    fsa.generateFile((this.PATH + "classdiagram.puml"), this.createClassDiagram(this.entities));
  }
  
  public CharSequence createDocumentation(final List<Entity> entities) {
    StringConcatenation _builder = new StringConcatenation();
    {
      int _size = entities.size();
      boolean _greaterThan = (_size > 0);
      if (_greaterThan) {
        _builder.append("# Documentation");
        _builder.newLine();
        _builder.newLine();
        _builder.append("## Application Conceptual Data Model");
        _builder.newLine();
        _builder.append("![Domain Diagram](classdiagram.png)\t");
        _builder.newLine();
        _builder.append("\t");
        _builder.newLine();
        _builder.append("## Entities");
        _builder.newLine();
        _builder.append("\t");
        _builder.newLine();
        {
          for(final Entity x : entities) {
            {
              Description _description = x.getDescription();
              boolean _tripleNotEquals = (_description != null);
              if (_tripleNotEquals) {
                _builder.append("* **");
                String _name = x.getName();
                _builder.append(_name);
                _builder.append("** : ");
                String _textfield = x.getDescription().getTextfield();
                _builder.append(_textfield);
                _builder.newLineIfNotEmpty();
              }
            }
          }
        }
        _builder.newLine();
        {
          for(final Entity x_1 : entities) {
            {
              if (((x_1.getFunctions() != null) && (x_1.getFunctions().size() > 0))) {
                _builder.append("## Functions of ");
                String _name_1 = x_1.getName();
                _builder.append(_name_1);
                _builder.append(":\t");
                _builder.newLineIfNotEmpty();
                {
                  EList<Function> _functions = x_1.getFunctions();
                  for(final Function f : _functions) {
                    int count = 0;
                    _builder.newLineIfNotEmpty();
                    {
                      Description _description_1 = f.getDescription();
                      boolean _tripleNotEquals_1 = (_description_1 != null);
                      if (_tripleNotEquals_1) {
                        _builder.append("* **");
                        String _name_2 = f.getName();
                        _builder.append(_name_2);
                        _builder.append(" (");
                        {
                          EList<String> _params = f.getParams();
                          for(final String p : _params) {
                            _builder.append(count = (count + 1));
                            _builder.append(p);
                            {
                              int _size_1 = f.getParams().size();
                              boolean _lessThan = (count < _size_1);
                              if (_lessThan) {
                                _builder.append(",");
                              }
                            }
                          }
                        }
                        _builder.append(")**: ");
                        String _textfield_1 = f.getDescription().getTextfield();
                        _builder.append(_textfield_1);
                        _builder.newLineIfNotEmpty();
                      } else {
                        _builder.append("* **");
                        String _name_3 = f.getName();
                        _builder.append(_name_3);
                        _builder.append(" (");
                        {
                          EList<String> _params_1 = f.getParams();
                          for(final String p_1 : _params_1) {
                            _builder.append(p_1);
                            _builder.append(" , ");
                          }
                        }
                        _builder.append(")**: - ");
                        _builder.newLineIfNotEmpty();
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("## Copyright");
    _builder.newLine();
    _builder.append("This lib was PowerRight by [SEON Application Lib Generator](");
    _builder.append(this.GITLAB_PATH);
    _builder.append(") ");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  public CharSequence createClassDiagram(final List<Entity> entities) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("@startuml");
    _builder.newLine();
    {
      int _size = entities.size();
      boolean _greaterThan = (_size > 0);
      if (_greaterThan) {
        {
          for(final Entity e : entities) {
            _builder.append("class ");
            String _name = e.getName();
            _builder.append(_name);
            _builder.append("{");
            _builder.newLineIfNotEmpty();
            {
              EList<Attribute> _attributes = e.getAttributes();
              for(final Attribute a : _attributes) {
                _builder.append("\t");
                String _type = a.getType();
                _builder.append(_type, "\t");
                _builder.append(":");
                String _name_1 = a.getName();
                _builder.append(_name_1, "\t");
                _builder.newLineIfNotEmpty();
              }
            }
            {
              EList<Function> _functions = e.getFunctions();
              for(final Function f : _functions) {
                _builder.append("\t");
                int count = 0;
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                String _type_1 = f.getType();
                _builder.append(_type_1, "\t");
                _builder.append(": ");
                String _name_2 = f.getName();
                _builder.append(_name_2, "\t");
                _builder.append("(");
                {
                  EList<String> _params = f.getParams();
                  for(final String p : _params) {
                    _builder.append(p, "\t");
                    _builder.append(count = (count + 1), "\t");
                    {
                      int _size_1 = f.getParams().size();
                      boolean _lessThan = (count < _size_1);
                      if (_lessThan) {
                        _builder.append(",");
                      }
                    }
                  }
                }
                _builder.append(")");
                _builder.newLineIfNotEmpty();
              }
            }
            _builder.append("}");
            _builder.newLine();
            {
              Entity _superType = e.getSuperType();
              boolean _tripleNotEquals = (_superType != null);
              if (_tripleNotEquals) {
                QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(e.getSuperType());
                _builder.append(_fullyQualifiedName);
                _builder.append(" <|-- ");
                String _name_3 = e.getName();
                _builder.append(_name_3);
              }
            }
            _builder.newLineIfNotEmpty();
            _builder.newLine();
            {
              EList<Relation> _relations = e.getRelations();
              for(final Relation r : _relations) {
                {
                  Class<?> _instanceClass = r.eClass().getInstanceClass();
                  boolean _equals = Objects.equal(_instanceClass, OneToOne.class);
                  if (_equals) {
                    String _name_4 = e.getName();
                    _builder.append(_name_4);
                    _builder.append(" \"1\" -- \"1\" ");
                    QualifiedName _fullyQualifiedName_1 = this._iQualifiedNameProvider.getFullyQualifiedName(r.getType());
                    _builder.append(_fullyQualifiedName_1);
                    _builder.append(" : ");
                    String _name_5 = r.getName();
                    _builder.append(_name_5);
                    _builder.append(" >");
                    _builder.newLineIfNotEmpty();
                  }
                }
                {
                  Class<?> _instanceClass_1 = r.eClass().getInstanceClass();
                  boolean _equals_1 = Objects.equal(_instanceClass_1, ManyToMany.class);
                  if (_equals_1) {
                    String _name_6 = e.getName();
                    _builder.append(_name_6);
                    _builder.append(" \"0..*\" -- \"0..*\" ");
                    QualifiedName _fullyQualifiedName_2 = this._iQualifiedNameProvider.getFullyQualifiedName(r.getType());
                    _builder.append(_fullyQualifiedName_2);
                    _builder.append(" : ");
                    String _name_7 = r.getName();
                    _builder.append(_name_7);
                    _builder.append(" >");
                    _builder.newLineIfNotEmpty();
                  }
                }
                {
                  Class<?> _instanceClass_2 = r.eClass().getInstanceClass();
                  boolean _equals_2 = Objects.equal(_instanceClass_2, OneToMany.class);
                  if (_equals_2) {
                    String _name_8 = e.getName();
                    _builder.append(_name_8);
                    _builder.append(" \"1\" -- \"0..*\" ");
                    QualifiedName _fullyQualifiedName_3 = this._iQualifiedNameProvider.getFullyQualifiedName(r.getType());
                    _builder.append(_fullyQualifiedName_3);
                    _builder.append(" : ");
                    String _name_9 = r.getName();
                    _builder.append(_name_9);
                    _builder.append(" >");
                    _builder.newLineIfNotEmpty();
                  }
                }
              }
            }
            _builder.newLine();
          }
        }
      }
    }
    _builder.newLine();
    _builder.append("@enduml");
    _builder.newLine();
    return _builder;
  }
}
