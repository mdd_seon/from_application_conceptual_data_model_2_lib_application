/*
 * generated by Xtext 2.21.0
 */
package br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.validation;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.validation.AbstractDeclarativeValidator;

public abstract class AbstractAcm2LaValidator extends AbstractDeclarativeValidator {
	
	@Override
	protected List<EPackage> getEPackages() {
		List<EPackage> result = new ArrayList<EPackage>();
		result.add(br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.Acm2LaPackage.eINSTANCE);
		return result;
	}
}
