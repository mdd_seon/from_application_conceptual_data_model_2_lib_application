package br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.services.Acm2LaGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalAcm2LaParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Configuration'", "'{'", "'}'", "'author:'", "'author_email:'", "'repository:'", "'lib_name:'", "'software:'", "'about:'", "'#'", "'entity'", "'extends'", "':'", "'OneToOne'", "'ManyToMany'", "'OneToMany'", "'function'", "'('", "','", "')'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalAcm2LaParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalAcm2LaParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalAcm2LaParser.tokenNames; }
    public String getGrammarFileName() { return "InternalAcm2La.g"; }



     	private Acm2LaGrammarAccess grammarAccess;

        public InternalAcm2LaParser(TokenStream input, Acm2LaGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Application";
       	}

       	@Override
       	protected Acm2LaGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleApplication"
    // InternalAcm2La.g:64:1: entryRuleApplication returns [EObject current=null] : iv_ruleApplication= ruleApplication EOF ;
    public final EObject entryRuleApplication() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleApplication = null;


        try {
            // InternalAcm2La.g:64:52: (iv_ruleApplication= ruleApplication EOF )
            // InternalAcm2La.g:65:2: iv_ruleApplication= ruleApplication EOF
            {
             newCompositeNode(grammarAccess.getApplicationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleApplication=ruleApplication();

            state._fsp--;

             current =iv_ruleApplication; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleApplication"


    // $ANTLR start "ruleApplication"
    // InternalAcm2La.g:71:1: ruleApplication returns [EObject current=null] : ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_elements_1_0= ruleEntity ) )* ) ;
    public final EObject ruleApplication() throws RecognitionException {
        EObject current = null;

        EObject lv_configuration_0_0 = null;

        EObject lv_elements_1_0 = null;



        	enterRule();

        try {
            // InternalAcm2La.g:77:2: ( ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_elements_1_0= ruleEntity ) )* ) )
            // InternalAcm2La.g:78:2: ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_elements_1_0= ruleEntity ) )* )
            {
            // InternalAcm2La.g:78:2: ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_elements_1_0= ruleEntity ) )* )
            // InternalAcm2La.g:79:3: ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_elements_1_0= ruleEntity ) )*
            {
            // InternalAcm2La.g:79:3: ( (lv_configuration_0_0= ruleConfiguration ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==11) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalAcm2La.g:80:4: (lv_configuration_0_0= ruleConfiguration )
                    {
                    // InternalAcm2La.g:80:4: (lv_configuration_0_0= ruleConfiguration )
                    // InternalAcm2La.g:81:5: lv_configuration_0_0= ruleConfiguration
                    {

                    					newCompositeNode(grammarAccess.getApplicationAccess().getConfigurationConfigurationParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_3);
                    lv_configuration_0_0=ruleConfiguration();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getApplicationRule());
                    					}
                    					set(
                    						current,
                    						"configuration",
                    						lv_configuration_0_0,
                    						"br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.Acm2La.Configuration");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalAcm2La.g:98:3: ( (lv_elements_1_0= ruleEntity ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>=20 && LA2_0<=21)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalAcm2La.g:99:4: (lv_elements_1_0= ruleEntity )
            	    {
            	    // InternalAcm2La.g:99:4: (lv_elements_1_0= ruleEntity )
            	    // InternalAcm2La.g:100:5: lv_elements_1_0= ruleEntity
            	    {

            	    					newCompositeNode(grammarAccess.getApplicationAccess().getElementsEntityParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_elements_1_0=ruleEntity();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getApplicationRule());
            	    					}
            	    					add(
            	    						current,
            	    						"elements",
            	    						lv_elements_1_0,
            	    						"br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.Acm2La.Entity");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleApplication"


    // $ANTLR start "entryRuleConfiguration"
    // InternalAcm2La.g:121:1: entryRuleConfiguration returns [EObject current=null] : iv_ruleConfiguration= ruleConfiguration EOF ;
    public final EObject entryRuleConfiguration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConfiguration = null;


        try {
            // InternalAcm2La.g:121:54: (iv_ruleConfiguration= ruleConfiguration EOF )
            // InternalAcm2La.g:122:2: iv_ruleConfiguration= ruleConfiguration EOF
            {
             newCompositeNode(grammarAccess.getConfigurationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConfiguration=ruleConfiguration();

            state._fsp--;

             current =iv_ruleConfiguration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConfiguration"


    // $ANTLR start "ruleConfiguration"
    // InternalAcm2La.g:128:1: ruleConfiguration returns [EObject current=null] : (otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_software_2_0= ruleSoftware ) ) ( (lv_about_3_0= ruleAbout ) ) ( (lv_lib_4_0= ruleLib ) ) ( (lv_author_5_0= ruleAuthor ) ) ( (lv_author_email_6_0= ruleAuthor_Email ) ) ( (lv_repository_7_0= ruleRepository ) ) otherlv_8= '}' ) ;
    public final EObject ruleConfiguration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_8=null;
        EObject lv_software_2_0 = null;

        EObject lv_about_3_0 = null;

        EObject lv_lib_4_0 = null;

        EObject lv_author_5_0 = null;

        EObject lv_author_email_6_0 = null;

        EObject lv_repository_7_0 = null;



        	enterRule();

        try {
            // InternalAcm2La.g:134:2: ( (otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_software_2_0= ruleSoftware ) ) ( (lv_about_3_0= ruleAbout ) ) ( (lv_lib_4_0= ruleLib ) ) ( (lv_author_5_0= ruleAuthor ) ) ( (lv_author_email_6_0= ruleAuthor_Email ) ) ( (lv_repository_7_0= ruleRepository ) ) otherlv_8= '}' ) )
            // InternalAcm2La.g:135:2: (otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_software_2_0= ruleSoftware ) ) ( (lv_about_3_0= ruleAbout ) ) ( (lv_lib_4_0= ruleLib ) ) ( (lv_author_5_0= ruleAuthor ) ) ( (lv_author_email_6_0= ruleAuthor_Email ) ) ( (lv_repository_7_0= ruleRepository ) ) otherlv_8= '}' )
            {
            // InternalAcm2La.g:135:2: (otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_software_2_0= ruleSoftware ) ) ( (lv_about_3_0= ruleAbout ) ) ( (lv_lib_4_0= ruleLib ) ) ( (lv_author_5_0= ruleAuthor ) ) ( (lv_author_email_6_0= ruleAuthor_Email ) ) ( (lv_repository_7_0= ruleRepository ) ) otherlv_8= '}' )
            // InternalAcm2La.g:136:3: otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_software_2_0= ruleSoftware ) ) ( (lv_about_3_0= ruleAbout ) ) ( (lv_lib_4_0= ruleLib ) ) ( (lv_author_5_0= ruleAuthor ) ) ( (lv_author_email_6_0= ruleAuthor_Email ) ) ( (lv_repository_7_0= ruleRepository ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getConfigurationAccess().getConfigurationKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalAcm2La.g:144:3: ( (lv_software_2_0= ruleSoftware ) )
            // InternalAcm2La.g:145:4: (lv_software_2_0= ruleSoftware )
            {
            // InternalAcm2La.g:145:4: (lv_software_2_0= ruleSoftware )
            // InternalAcm2La.g:146:5: lv_software_2_0= ruleSoftware
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getSoftwareSoftwareParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_6);
            lv_software_2_0=ruleSoftware();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"software",
            						lv_software_2_0,
            						"br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.Acm2La.Software");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAcm2La.g:163:3: ( (lv_about_3_0= ruleAbout ) )
            // InternalAcm2La.g:164:4: (lv_about_3_0= ruleAbout )
            {
            // InternalAcm2La.g:164:4: (lv_about_3_0= ruleAbout )
            // InternalAcm2La.g:165:5: lv_about_3_0= ruleAbout
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getAboutAboutParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_7);
            lv_about_3_0=ruleAbout();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"about",
            						lv_about_3_0,
            						"br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.Acm2La.About");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAcm2La.g:182:3: ( (lv_lib_4_0= ruleLib ) )
            // InternalAcm2La.g:183:4: (lv_lib_4_0= ruleLib )
            {
            // InternalAcm2La.g:183:4: (lv_lib_4_0= ruleLib )
            // InternalAcm2La.g:184:5: lv_lib_4_0= ruleLib
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getLibLibParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_8);
            lv_lib_4_0=ruleLib();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"lib",
            						lv_lib_4_0,
            						"br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.Acm2La.Lib");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAcm2La.g:201:3: ( (lv_author_5_0= ruleAuthor ) )
            // InternalAcm2La.g:202:4: (lv_author_5_0= ruleAuthor )
            {
            // InternalAcm2La.g:202:4: (lv_author_5_0= ruleAuthor )
            // InternalAcm2La.g:203:5: lv_author_5_0= ruleAuthor
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getAuthorAuthorParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_9);
            lv_author_5_0=ruleAuthor();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"author",
            						lv_author_5_0,
            						"br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.Acm2La.Author");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAcm2La.g:220:3: ( (lv_author_email_6_0= ruleAuthor_Email ) )
            // InternalAcm2La.g:221:4: (lv_author_email_6_0= ruleAuthor_Email )
            {
            // InternalAcm2La.g:221:4: (lv_author_email_6_0= ruleAuthor_Email )
            // InternalAcm2La.g:222:5: lv_author_email_6_0= ruleAuthor_Email
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getAuthor_emailAuthor_EmailParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_10);
            lv_author_email_6_0=ruleAuthor_Email();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"author_email",
            						lv_author_email_6_0,
            						"br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.Acm2La.Author_Email");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAcm2La.g:239:3: ( (lv_repository_7_0= ruleRepository ) )
            // InternalAcm2La.g:240:4: (lv_repository_7_0= ruleRepository )
            {
            // InternalAcm2La.g:240:4: (lv_repository_7_0= ruleRepository )
            // InternalAcm2La.g:241:5: lv_repository_7_0= ruleRepository
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getRepositoryRepositoryParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_11);
            lv_repository_7_0=ruleRepository();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"repository",
            						lv_repository_7_0,
            						"br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.Acm2La.Repository");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_8=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConfiguration"


    // $ANTLR start "entryRuleAuthor"
    // InternalAcm2La.g:266:1: entryRuleAuthor returns [EObject current=null] : iv_ruleAuthor= ruleAuthor EOF ;
    public final EObject entryRuleAuthor() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAuthor = null;


        try {
            // InternalAcm2La.g:266:47: (iv_ruleAuthor= ruleAuthor EOF )
            // InternalAcm2La.g:267:2: iv_ruleAuthor= ruleAuthor EOF
            {
             newCompositeNode(grammarAccess.getAuthorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAuthor=ruleAuthor();

            state._fsp--;

             current =iv_ruleAuthor; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAuthor"


    // $ANTLR start "ruleAuthor"
    // InternalAcm2La.g:273:1: ruleAuthor returns [EObject current=null] : (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleAuthor() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalAcm2La.g:279:2: ( (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalAcm2La.g:280:2: (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalAcm2La.g:280:2: (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalAcm2La.g:281:3: otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,14,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getAuthorAccess().getAuthorKeyword_0());
            		
            // InternalAcm2La.g:285:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalAcm2La.g:286:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalAcm2La.g:286:4: (lv_name_1_0= RULE_STRING )
            // InternalAcm2La.g:287:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAuthorAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAuthorRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAuthor"


    // $ANTLR start "entryRuleAuthor_Email"
    // InternalAcm2La.g:307:1: entryRuleAuthor_Email returns [EObject current=null] : iv_ruleAuthor_Email= ruleAuthor_Email EOF ;
    public final EObject entryRuleAuthor_Email() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAuthor_Email = null;


        try {
            // InternalAcm2La.g:307:53: (iv_ruleAuthor_Email= ruleAuthor_Email EOF )
            // InternalAcm2La.g:308:2: iv_ruleAuthor_Email= ruleAuthor_Email EOF
            {
             newCompositeNode(grammarAccess.getAuthor_EmailRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAuthor_Email=ruleAuthor_Email();

            state._fsp--;

             current =iv_ruleAuthor_Email; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAuthor_Email"


    // $ANTLR start "ruleAuthor_Email"
    // InternalAcm2La.g:314:1: ruleAuthor_Email returns [EObject current=null] : (otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleAuthor_Email() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalAcm2La.g:320:2: ( (otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalAcm2La.g:321:2: (otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalAcm2La.g:321:2: (otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalAcm2La.g:322:3: otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,15,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getAuthor_EmailAccess().getAuthor_emailKeyword_0());
            		
            // InternalAcm2La.g:326:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalAcm2La.g:327:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalAcm2La.g:327:4: (lv_name_1_0= RULE_STRING )
            // InternalAcm2La.g:328:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAuthor_EmailAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAuthor_EmailRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAuthor_Email"


    // $ANTLR start "entryRuleRepository"
    // InternalAcm2La.g:348:1: entryRuleRepository returns [EObject current=null] : iv_ruleRepository= ruleRepository EOF ;
    public final EObject entryRuleRepository() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRepository = null;


        try {
            // InternalAcm2La.g:348:51: (iv_ruleRepository= ruleRepository EOF )
            // InternalAcm2La.g:349:2: iv_ruleRepository= ruleRepository EOF
            {
             newCompositeNode(grammarAccess.getRepositoryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRepository=ruleRepository();

            state._fsp--;

             current =iv_ruleRepository; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRepository"


    // $ANTLR start "ruleRepository"
    // InternalAcm2La.g:355:1: ruleRepository returns [EObject current=null] : (otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleRepository() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalAcm2La.g:361:2: ( (otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalAcm2La.g:362:2: (otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalAcm2La.g:362:2: (otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalAcm2La.g:363:3: otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,16,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getRepositoryAccess().getRepositoryKeyword_0());
            		
            // InternalAcm2La.g:367:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalAcm2La.g:368:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalAcm2La.g:368:4: (lv_name_1_0= RULE_STRING )
            // InternalAcm2La.g:369:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getRepositoryAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getRepositoryRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRepository"


    // $ANTLR start "entryRuleLib"
    // InternalAcm2La.g:389:1: entryRuleLib returns [EObject current=null] : iv_ruleLib= ruleLib EOF ;
    public final EObject entryRuleLib() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLib = null;


        try {
            // InternalAcm2La.g:389:44: (iv_ruleLib= ruleLib EOF )
            // InternalAcm2La.g:390:2: iv_ruleLib= ruleLib EOF
            {
             newCompositeNode(grammarAccess.getLibRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLib=ruleLib();

            state._fsp--;

             current =iv_ruleLib; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLib"


    // $ANTLR start "ruleLib"
    // InternalAcm2La.g:396:1: ruleLib returns [EObject current=null] : (otherlv_0= 'lib_name:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleLib() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalAcm2La.g:402:2: ( (otherlv_0= 'lib_name:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalAcm2La.g:403:2: (otherlv_0= 'lib_name:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalAcm2La.g:403:2: (otherlv_0= 'lib_name:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalAcm2La.g:404:3: otherlv_0= 'lib_name:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,17,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getLibAccess().getLib_nameKeyword_0());
            		
            // InternalAcm2La.g:408:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalAcm2La.g:409:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalAcm2La.g:409:4: (lv_name_1_0= RULE_STRING )
            // InternalAcm2La.g:410:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getLibAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getLibRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLib"


    // $ANTLR start "entryRuleSoftware"
    // InternalAcm2La.g:430:1: entryRuleSoftware returns [EObject current=null] : iv_ruleSoftware= ruleSoftware EOF ;
    public final EObject entryRuleSoftware() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSoftware = null;


        try {
            // InternalAcm2La.g:430:49: (iv_ruleSoftware= ruleSoftware EOF )
            // InternalAcm2La.g:431:2: iv_ruleSoftware= ruleSoftware EOF
            {
             newCompositeNode(grammarAccess.getSoftwareRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSoftware=ruleSoftware();

            state._fsp--;

             current =iv_ruleSoftware; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSoftware"


    // $ANTLR start "ruleSoftware"
    // InternalAcm2La.g:437:1: ruleSoftware returns [EObject current=null] : (otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleSoftware() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalAcm2La.g:443:2: ( (otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalAcm2La.g:444:2: (otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalAcm2La.g:444:2: (otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalAcm2La.g:445:3: otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,18,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getSoftwareAccess().getSoftwareKeyword_0());
            		
            // InternalAcm2La.g:449:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalAcm2La.g:450:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalAcm2La.g:450:4: (lv_name_1_0= RULE_STRING )
            // InternalAcm2La.g:451:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getSoftwareAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSoftwareRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSoftware"


    // $ANTLR start "entryRuleAbout"
    // InternalAcm2La.g:471:1: entryRuleAbout returns [EObject current=null] : iv_ruleAbout= ruleAbout EOF ;
    public final EObject entryRuleAbout() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbout = null;


        try {
            // InternalAcm2La.g:471:46: (iv_ruleAbout= ruleAbout EOF )
            // InternalAcm2La.g:472:2: iv_ruleAbout= ruleAbout EOF
            {
             newCompositeNode(grammarAccess.getAboutRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAbout=ruleAbout();

            state._fsp--;

             current =iv_ruleAbout; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbout"


    // $ANTLR start "ruleAbout"
    // InternalAcm2La.g:478:1: ruleAbout returns [EObject current=null] : (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleAbout() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalAcm2La.g:484:2: ( (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalAcm2La.g:485:2: (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalAcm2La.g:485:2: (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalAcm2La.g:486:3: otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,19,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getAboutAccess().getAboutKeyword_0());
            		
            // InternalAcm2La.g:490:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalAcm2La.g:491:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalAcm2La.g:491:4: (lv_name_1_0= RULE_STRING )
            // InternalAcm2La.g:492:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAboutAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAboutRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbout"


    // $ANTLR start "entryRuleDescription"
    // InternalAcm2La.g:512:1: entryRuleDescription returns [EObject current=null] : iv_ruleDescription= ruleDescription EOF ;
    public final EObject entryRuleDescription() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDescription = null;


        try {
            // InternalAcm2La.g:512:52: (iv_ruleDescription= ruleDescription EOF )
            // InternalAcm2La.g:513:2: iv_ruleDescription= ruleDescription EOF
            {
             newCompositeNode(grammarAccess.getDescriptionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDescription=ruleDescription();

            state._fsp--;

             current =iv_ruleDescription; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDescription"


    // $ANTLR start "ruleDescription"
    // InternalAcm2La.g:519:1: ruleDescription returns [EObject current=null] : (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleDescription() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_textfield_1_0=null;


        	enterRule();

        try {
            // InternalAcm2La.g:525:2: ( (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) ) )
            // InternalAcm2La.g:526:2: (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) )
            {
            // InternalAcm2La.g:526:2: (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) )
            // InternalAcm2La.g:527:3: otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,20,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getDescriptionAccess().getNumberSignKeyword_0());
            		
            // InternalAcm2La.g:531:3: ( (lv_textfield_1_0= RULE_STRING ) )
            // InternalAcm2La.g:532:4: (lv_textfield_1_0= RULE_STRING )
            {
            // InternalAcm2La.g:532:4: (lv_textfield_1_0= RULE_STRING )
            // InternalAcm2La.g:533:5: lv_textfield_1_0= RULE_STRING
            {
            lv_textfield_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_textfield_1_0, grammarAccess.getDescriptionAccess().getTextfieldSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDescriptionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"textfield",
            						lv_textfield_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDescription"


    // $ANTLR start "entryRuleEntity"
    // InternalAcm2La.g:553:1: entryRuleEntity returns [EObject current=null] : iv_ruleEntity= ruleEntity EOF ;
    public final EObject entryRuleEntity() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEntity = null;


        try {
            // InternalAcm2La.g:553:47: (iv_ruleEntity= ruleEntity EOF )
            // InternalAcm2La.g:554:2: iv_ruleEntity= ruleEntity EOF
            {
             newCompositeNode(grammarAccess.getEntityRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEntity=ruleEntity();

            state._fsp--;

             current =iv_ruleEntity; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEntity"


    // $ANTLR start "ruleEntity"
    // InternalAcm2La.g:560:1: ruleEntity returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( (otherlv_4= RULE_ID ) ) )? otherlv_5= '{' ( (lv_attributes_6_0= ruleAttribute ) )* ( (lv_functions_7_0= ruleFunction ) )* ( (lv_relations_8_0= ruleRelation ) )* otherlv_9= '}' ) ;
    public final EObject ruleEntity() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_9=null;
        EObject lv_description_0_0 = null;

        EObject lv_attributes_6_0 = null;

        EObject lv_functions_7_0 = null;

        EObject lv_relations_8_0 = null;



        	enterRule();

        try {
            // InternalAcm2La.g:566:2: ( ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( (otherlv_4= RULE_ID ) ) )? otherlv_5= '{' ( (lv_attributes_6_0= ruleAttribute ) )* ( (lv_functions_7_0= ruleFunction ) )* ( (lv_relations_8_0= ruleRelation ) )* otherlv_9= '}' ) )
            // InternalAcm2La.g:567:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( (otherlv_4= RULE_ID ) ) )? otherlv_5= '{' ( (lv_attributes_6_0= ruleAttribute ) )* ( (lv_functions_7_0= ruleFunction ) )* ( (lv_relations_8_0= ruleRelation ) )* otherlv_9= '}' )
            {
            // InternalAcm2La.g:567:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( (otherlv_4= RULE_ID ) ) )? otherlv_5= '{' ( (lv_attributes_6_0= ruleAttribute ) )* ( (lv_functions_7_0= ruleFunction ) )* ( (lv_relations_8_0= ruleRelation ) )* otherlv_9= '}' )
            // InternalAcm2La.g:568:3: ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( (otherlv_4= RULE_ID ) ) )? otherlv_5= '{' ( (lv_attributes_6_0= ruleAttribute ) )* ( (lv_functions_7_0= ruleFunction ) )* ( (lv_relations_8_0= ruleRelation ) )* otherlv_9= '}'
            {
            // InternalAcm2La.g:568:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==20) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalAcm2La.g:569:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalAcm2La.g:569:4: (lv_description_0_0= ruleDescription )
                    // InternalAcm2La.g:570:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getEntityAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_13);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getEntityRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.Acm2La.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,21,FOLLOW_14); 

            			newLeafNode(otherlv_1, grammarAccess.getEntityAccess().getEntityKeyword_1());
            		
            // InternalAcm2La.g:591:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalAcm2La.g:592:4: (lv_name_2_0= RULE_ID )
            {
            // InternalAcm2La.g:592:4: (lv_name_2_0= RULE_ID )
            // InternalAcm2La.g:593:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_15); 

            					newLeafNode(lv_name_2_0, grammarAccess.getEntityAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEntityRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalAcm2La.g:609:3: (otherlv_3= 'extends' ( (otherlv_4= RULE_ID ) ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==22) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalAcm2La.g:610:4: otherlv_3= 'extends' ( (otherlv_4= RULE_ID ) )
                    {
                    otherlv_3=(Token)match(input,22,FOLLOW_14); 

                    				newLeafNode(otherlv_3, grammarAccess.getEntityAccess().getExtendsKeyword_3_0());
                    			
                    // InternalAcm2La.g:614:4: ( (otherlv_4= RULE_ID ) )
                    // InternalAcm2La.g:615:5: (otherlv_4= RULE_ID )
                    {
                    // InternalAcm2La.g:615:5: (otherlv_4= RULE_ID )
                    // InternalAcm2La.g:616:6: otherlv_4= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getEntityRule());
                    						}
                    					
                    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_4); 

                    						newLeafNode(otherlv_4, grammarAccess.getEntityAccess().getSuperTypeEntityCrossReference_3_1_0());
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,12,FOLLOW_16); 

            			newLeafNode(otherlv_5, grammarAccess.getEntityAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalAcm2La.g:632:3: ( (lv_attributes_6_0= ruleAttribute ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==20) ) {
                    int LA5_1 = input.LA(2);

                    if ( (LA5_1==RULE_STRING) ) {
                        int LA5_4 = input.LA(3);

                        if ( (LA5_4==RULE_ID) ) {
                            alt5=1;
                        }


                    }


                }
                else if ( (LA5_0==RULE_ID) ) {
                    int LA5_3 = input.LA(2);

                    if ( (LA5_3==23) ) {
                        alt5=1;
                    }


                }


                switch (alt5) {
            	case 1 :
            	    // InternalAcm2La.g:633:4: (lv_attributes_6_0= ruleAttribute )
            	    {
            	    // InternalAcm2La.g:633:4: (lv_attributes_6_0= ruleAttribute )
            	    // InternalAcm2La.g:634:5: lv_attributes_6_0= ruleAttribute
            	    {

            	    					newCompositeNode(grammarAccess.getEntityAccess().getAttributesAttributeParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_16);
            	    lv_attributes_6_0=ruleAttribute();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEntityRule());
            	    					}
            	    					add(
            	    						current,
            	    						"attributes",
            	    						lv_attributes_6_0,
            	    						"br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.Acm2La.Attribute");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            // InternalAcm2La.g:651:3: ( (lv_functions_7_0= ruleFunction ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==20||LA6_0==27) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalAcm2La.g:652:4: (lv_functions_7_0= ruleFunction )
            	    {
            	    // InternalAcm2La.g:652:4: (lv_functions_7_0= ruleFunction )
            	    // InternalAcm2La.g:653:5: lv_functions_7_0= ruleFunction
            	    {

            	    					newCompositeNode(grammarAccess.getEntityAccess().getFunctionsFunctionParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_16);
            	    lv_functions_7_0=ruleFunction();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEntityRule());
            	    					}
            	    					add(
            	    						current,
            	    						"functions",
            	    						lv_functions_7_0,
            	    						"br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.Acm2La.Function");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            // InternalAcm2La.g:670:3: ( (lv_relations_8_0= ruleRelation ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==RULE_ID) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalAcm2La.g:671:4: (lv_relations_8_0= ruleRelation )
            	    {
            	    // InternalAcm2La.g:671:4: (lv_relations_8_0= ruleRelation )
            	    // InternalAcm2La.g:672:5: lv_relations_8_0= ruleRelation
            	    {

            	    					newCompositeNode(grammarAccess.getEntityAccess().getRelationsRelationParserRuleCall_7_0());
            	    				
            	    pushFollow(FOLLOW_17);
            	    lv_relations_8_0=ruleRelation();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEntityRule());
            	    					}
            	    					add(
            	    						current,
            	    						"relations",
            	    						lv_relations_8_0,
            	    						"br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.Acm2La.Relation");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            otherlv_9=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getEntityAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEntity"


    // $ANTLR start "entryRuleAttribute"
    // InternalAcm2La.g:697:1: entryRuleAttribute returns [EObject current=null] : iv_ruleAttribute= ruleAttribute EOF ;
    public final EObject entryRuleAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttribute = null;


        try {
            // InternalAcm2La.g:697:50: (iv_ruleAttribute= ruleAttribute EOF )
            // InternalAcm2La.g:698:2: iv_ruleAttribute= ruleAttribute EOF
            {
             newCompositeNode(grammarAccess.getAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttribute=ruleAttribute();

            state._fsp--;

             current =iv_ruleAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // InternalAcm2La.g:704:1: ruleAttribute returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_ID ) ) ) ;
    public final EObject ruleAttribute() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_type_3_0=null;
        EObject lv_description_0_0 = null;



        	enterRule();

        try {
            // InternalAcm2La.g:710:2: ( ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_ID ) ) ) )
            // InternalAcm2La.g:711:2: ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_ID ) ) )
            {
            // InternalAcm2La.g:711:2: ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_ID ) ) )
            // InternalAcm2La.g:712:3: ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_ID ) )
            {
            // InternalAcm2La.g:712:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==20) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalAcm2La.g:713:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalAcm2La.g:713:4: (lv_description_0_0= ruleDescription )
                    // InternalAcm2La.g:714:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getAttributeAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_14);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getAttributeRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.Acm2La.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalAcm2La.g:731:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalAcm2La.g:732:4: (lv_name_1_0= RULE_ID )
            {
            // InternalAcm2La.g:732:4: (lv_name_1_0= RULE_ID )
            // InternalAcm2La.g:733:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_18); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAttributeAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAttributeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,23,FOLLOW_14); 

            			newLeafNode(otherlv_2, grammarAccess.getAttributeAccess().getColonKeyword_2());
            		
            // InternalAcm2La.g:753:3: ( (lv_type_3_0= RULE_ID ) )
            // InternalAcm2La.g:754:4: (lv_type_3_0= RULE_ID )
            {
            // InternalAcm2La.g:754:4: (lv_type_3_0= RULE_ID )
            // InternalAcm2La.g:755:5: lv_type_3_0= RULE_ID
            {
            lv_type_3_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_type_3_0, grammarAccess.getAttributeAccess().getTypeIDTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAttributeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"type",
            						lv_type_3_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleRelation"
    // InternalAcm2La.g:775:1: entryRuleRelation returns [EObject current=null] : iv_ruleRelation= ruleRelation EOF ;
    public final EObject entryRuleRelation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelation = null;


        try {
            // InternalAcm2La.g:775:49: (iv_ruleRelation= ruleRelation EOF )
            // InternalAcm2La.g:776:2: iv_ruleRelation= ruleRelation EOF
            {
             newCompositeNode(grammarAccess.getRelationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRelation=ruleRelation();

            state._fsp--;

             current =iv_ruleRelation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelation"


    // $ANTLR start "ruleRelation"
    // InternalAcm2La.g:782:1: ruleRelation returns [EObject current=null] : (this_OneToOne_0= ruleOneToOne | this_ManyToMany_1= ruleManyToMany | this_OneToMany_2= ruleOneToMany ) ;
    public final EObject ruleRelation() throws RecognitionException {
        EObject current = null;

        EObject this_OneToOne_0 = null;

        EObject this_ManyToMany_1 = null;

        EObject this_OneToMany_2 = null;



        	enterRule();

        try {
            // InternalAcm2La.g:788:2: ( (this_OneToOne_0= ruleOneToOne | this_ManyToMany_1= ruleManyToMany | this_OneToMany_2= ruleOneToMany ) )
            // InternalAcm2La.g:789:2: (this_OneToOne_0= ruleOneToOne | this_ManyToMany_1= ruleManyToMany | this_OneToMany_2= ruleOneToMany )
            {
            // InternalAcm2La.g:789:2: (this_OneToOne_0= ruleOneToOne | this_ManyToMany_1= ruleManyToMany | this_OneToMany_2= ruleOneToMany )
            int alt9=3;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_ID) ) {
                switch ( input.LA(2) ) {
                case 26:
                    {
                    alt9=3;
                    }
                    break;
                case 24:
                    {
                    alt9=1;
                    }
                    break;
                case 25:
                    {
                    alt9=2;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 9, 1, input);

                    throw nvae;
                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalAcm2La.g:790:3: this_OneToOne_0= ruleOneToOne
                    {

                    			newCompositeNode(grammarAccess.getRelationAccess().getOneToOneParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_OneToOne_0=ruleOneToOne();

                    state._fsp--;


                    			current = this_OneToOne_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalAcm2La.g:799:3: this_ManyToMany_1= ruleManyToMany
                    {

                    			newCompositeNode(grammarAccess.getRelationAccess().getManyToManyParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ManyToMany_1=ruleManyToMany();

                    state._fsp--;


                    			current = this_ManyToMany_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalAcm2La.g:808:3: this_OneToMany_2= ruleOneToMany
                    {

                    			newCompositeNode(grammarAccess.getRelationAccess().getOneToManyParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_OneToMany_2=ruleOneToMany();

                    state._fsp--;


                    			current = this_OneToMany_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelation"


    // $ANTLR start "entryRuleOneToOne"
    // InternalAcm2La.g:820:1: entryRuleOneToOne returns [EObject current=null] : iv_ruleOneToOne= ruleOneToOne EOF ;
    public final EObject entryRuleOneToOne() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOneToOne = null;


        try {
            // InternalAcm2La.g:820:49: (iv_ruleOneToOne= ruleOneToOne EOF )
            // InternalAcm2La.g:821:2: iv_ruleOneToOne= ruleOneToOne EOF
            {
             newCompositeNode(grammarAccess.getOneToOneRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOneToOne=ruleOneToOne();

            state._fsp--;

             current =iv_ruleOneToOne; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOneToOne"


    // $ANTLR start "ruleOneToOne"
    // InternalAcm2La.g:827:1: ruleOneToOne returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleOneToOne() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalAcm2La.g:833:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( (otherlv_2= RULE_ID ) ) ) )
            // InternalAcm2La.g:834:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( (otherlv_2= RULE_ID ) ) )
            {
            // InternalAcm2La.g:834:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( (otherlv_2= RULE_ID ) ) )
            // InternalAcm2La.g:835:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( (otherlv_2= RULE_ID ) )
            {
            // InternalAcm2La.g:835:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalAcm2La.g:836:4: (lv_name_0_0= RULE_ID )
            {
            // InternalAcm2La.g:836:4: (lv_name_0_0= RULE_ID )
            // InternalAcm2La.g:837:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_19); 

            					newLeafNode(lv_name_0_0, grammarAccess.getOneToOneAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOneToOneRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,24,FOLLOW_14); 

            			newLeafNode(otherlv_1, grammarAccess.getOneToOneAccess().getOneToOneKeyword_1());
            		
            // InternalAcm2La.g:857:3: ( (otherlv_2= RULE_ID ) )
            // InternalAcm2La.g:858:4: (otherlv_2= RULE_ID )
            {
            // InternalAcm2La.g:858:4: (otherlv_2= RULE_ID )
            // InternalAcm2La.g:859:5: otherlv_2= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOneToOneRule());
            					}
            				
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_2, grammarAccess.getOneToOneAccess().getTypeEntityCrossReference_2_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOneToOne"


    // $ANTLR start "entryRuleManyToMany"
    // InternalAcm2La.g:874:1: entryRuleManyToMany returns [EObject current=null] : iv_ruleManyToMany= ruleManyToMany EOF ;
    public final EObject entryRuleManyToMany() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleManyToMany = null;


        try {
            // InternalAcm2La.g:874:51: (iv_ruleManyToMany= ruleManyToMany EOF )
            // InternalAcm2La.g:875:2: iv_ruleManyToMany= ruleManyToMany EOF
            {
             newCompositeNode(grammarAccess.getManyToManyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleManyToMany=ruleManyToMany();

            state._fsp--;

             current =iv_ruleManyToMany; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleManyToMany"


    // $ANTLR start "ruleManyToMany"
    // InternalAcm2La.g:881:1: ruleManyToMany returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleManyToMany() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalAcm2La.g:887:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( (otherlv_2= RULE_ID ) ) ) )
            // InternalAcm2La.g:888:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( (otherlv_2= RULE_ID ) ) )
            {
            // InternalAcm2La.g:888:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( (otherlv_2= RULE_ID ) ) )
            // InternalAcm2La.g:889:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( (otherlv_2= RULE_ID ) )
            {
            // InternalAcm2La.g:889:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalAcm2La.g:890:4: (lv_name_0_0= RULE_ID )
            {
            // InternalAcm2La.g:890:4: (lv_name_0_0= RULE_ID )
            // InternalAcm2La.g:891:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_20); 

            					newLeafNode(lv_name_0_0, grammarAccess.getManyToManyAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getManyToManyRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,25,FOLLOW_14); 

            			newLeafNode(otherlv_1, grammarAccess.getManyToManyAccess().getManyToManyKeyword_1());
            		
            // InternalAcm2La.g:911:3: ( (otherlv_2= RULE_ID ) )
            // InternalAcm2La.g:912:4: (otherlv_2= RULE_ID )
            {
            // InternalAcm2La.g:912:4: (otherlv_2= RULE_ID )
            // InternalAcm2La.g:913:5: otherlv_2= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getManyToManyRule());
            					}
            				
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_2, grammarAccess.getManyToManyAccess().getTypeEntityCrossReference_2_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleManyToMany"


    // $ANTLR start "entryRuleOneToMany"
    // InternalAcm2La.g:928:1: entryRuleOneToMany returns [EObject current=null] : iv_ruleOneToMany= ruleOneToMany EOF ;
    public final EObject entryRuleOneToMany() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOneToMany = null;


        try {
            // InternalAcm2La.g:928:50: (iv_ruleOneToMany= ruleOneToMany EOF )
            // InternalAcm2La.g:929:2: iv_ruleOneToMany= ruleOneToMany EOF
            {
             newCompositeNode(grammarAccess.getOneToManyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOneToMany=ruleOneToMany();

            state._fsp--;

             current =iv_ruleOneToMany; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOneToMany"


    // $ANTLR start "ruleOneToMany"
    // InternalAcm2La.g:935:1: ruleOneToMany returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleOneToMany() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalAcm2La.g:941:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( (otherlv_2= RULE_ID ) ) ) )
            // InternalAcm2La.g:942:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( (otherlv_2= RULE_ID ) ) )
            {
            // InternalAcm2La.g:942:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( (otherlv_2= RULE_ID ) ) )
            // InternalAcm2La.g:943:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( (otherlv_2= RULE_ID ) )
            {
            // InternalAcm2La.g:943:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalAcm2La.g:944:4: (lv_name_0_0= RULE_ID )
            {
            // InternalAcm2La.g:944:4: (lv_name_0_0= RULE_ID )
            // InternalAcm2La.g:945:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_21); 

            					newLeafNode(lv_name_0_0, grammarAccess.getOneToManyAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOneToManyRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,26,FOLLOW_14); 

            			newLeafNode(otherlv_1, grammarAccess.getOneToManyAccess().getOneToManyKeyword_1());
            		
            // InternalAcm2La.g:965:3: ( (otherlv_2= RULE_ID ) )
            // InternalAcm2La.g:966:4: (otherlv_2= RULE_ID )
            {
            // InternalAcm2La.g:966:4: (otherlv_2= RULE_ID )
            // InternalAcm2La.g:967:5: otherlv_2= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOneToManyRule());
            					}
            				
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_2, grammarAccess.getOneToManyAccess().getTypeEntityCrossReference_2_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOneToMany"


    // $ANTLR start "entryRuleFunction"
    // InternalAcm2La.g:982:1: entryRuleFunction returns [EObject current=null] : iv_ruleFunction= ruleFunction EOF ;
    public final EObject entryRuleFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunction = null;


        try {
            // InternalAcm2La.g:982:49: (iv_ruleFunction= ruleFunction EOF )
            // InternalAcm2La.g:983:2: iv_ruleFunction= ruleFunction EOF
            {
             newCompositeNode(grammarAccess.getFunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFunction=ruleFunction();

            state._fsp--;

             current =iv_ruleFunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // InternalAcm2La.g:989:1: ruleFunction returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= RULE_ID ) ) ) ;
    public final EObject ruleFunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token lv_params_4_0=null;
        Token otherlv_5=null;
        Token lv_params_6_0=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token lv_type_9_0=null;
        EObject lv_description_0_0 = null;



        	enterRule();

        try {
            // InternalAcm2La.g:995:2: ( ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= RULE_ID ) ) ) )
            // InternalAcm2La.g:996:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= RULE_ID ) ) )
            {
            // InternalAcm2La.g:996:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= RULE_ID ) ) )
            // InternalAcm2La.g:997:3: ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= RULE_ID ) )
            {
            // InternalAcm2La.g:997:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==20) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalAcm2La.g:998:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalAcm2La.g:998:4: (lv_description_0_0= ruleDescription )
                    // InternalAcm2La.g:999:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getFunctionAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_22);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getFunctionRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.Acm2La.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,27,FOLLOW_14); 

            			newLeafNode(otherlv_1, grammarAccess.getFunctionAccess().getFunctionKeyword_1());
            		
            // InternalAcm2La.g:1020:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalAcm2La.g:1021:4: (lv_name_2_0= RULE_ID )
            {
            // InternalAcm2La.g:1021:4: (lv_name_2_0= RULE_ID )
            // InternalAcm2La.g:1022:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_23); 

            					newLeafNode(lv_name_2_0, grammarAccess.getFunctionAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFunctionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,28,FOLLOW_24); 

            			newLeafNode(otherlv_3, grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_3());
            		
            // InternalAcm2La.g:1042:3: ( ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )* )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==RULE_ID) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalAcm2La.g:1043:4: ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )*
                    {
                    // InternalAcm2La.g:1043:4: ( (lv_params_4_0= RULE_ID ) )
                    // InternalAcm2La.g:1044:5: (lv_params_4_0= RULE_ID )
                    {
                    // InternalAcm2La.g:1044:5: (lv_params_4_0= RULE_ID )
                    // InternalAcm2La.g:1045:6: lv_params_4_0= RULE_ID
                    {
                    lv_params_4_0=(Token)match(input,RULE_ID,FOLLOW_25); 

                    						newLeafNode(lv_params_4_0, grammarAccess.getFunctionAccess().getParamsIDTerminalRuleCall_4_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFunctionRule());
                    						}
                    						addWithLastConsumed(
                    							current,
                    							"params",
                    							lv_params_4_0,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }


                    }

                    // InternalAcm2La.g:1061:4: (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )*
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( (LA11_0==29) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // InternalAcm2La.g:1062:5: otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) )
                    	    {
                    	    otherlv_5=(Token)match(input,29,FOLLOW_14); 

                    	    					newLeafNode(otherlv_5, grammarAccess.getFunctionAccess().getCommaKeyword_4_1_0());
                    	    				
                    	    // InternalAcm2La.g:1066:5: ( (lv_params_6_0= RULE_ID ) )
                    	    // InternalAcm2La.g:1067:6: (lv_params_6_0= RULE_ID )
                    	    {
                    	    // InternalAcm2La.g:1067:6: (lv_params_6_0= RULE_ID )
                    	    // InternalAcm2La.g:1068:7: lv_params_6_0= RULE_ID
                    	    {
                    	    lv_params_6_0=(Token)match(input,RULE_ID,FOLLOW_25); 

                    	    							newLeafNode(lv_params_6_0, grammarAccess.getFunctionAccess().getParamsIDTerminalRuleCall_4_1_1_0());
                    	    						

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getFunctionRule());
                    	    							}
                    	    							addWithLastConsumed(
                    	    								current,
                    	    								"params",
                    	    								lv_params_6_0,
                    	    								"org.eclipse.xtext.common.Terminals.ID");
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop11;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_7=(Token)match(input,30,FOLLOW_18); 

            			newLeafNode(otherlv_7, grammarAccess.getFunctionAccess().getRightParenthesisKeyword_5());
            		
            otherlv_8=(Token)match(input,23,FOLLOW_14); 

            			newLeafNode(otherlv_8, grammarAccess.getFunctionAccess().getColonKeyword_6());
            		
            // InternalAcm2La.g:1094:3: ( (lv_type_9_0= RULE_ID ) )
            // InternalAcm2La.g:1095:4: (lv_type_9_0= RULE_ID )
            {
            // InternalAcm2La.g:1095:4: (lv_type_9_0= RULE_ID )
            // InternalAcm2La.g:1096:5: lv_type_9_0= RULE_ID
            {
            lv_type_9_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_type_9_0, grammarAccess.getFunctionAccess().getTypeIDTerminalRuleCall_7_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFunctionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"type",
            						lv_type_9_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunction"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000300002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000401000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000008102020L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000002020L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000040000020L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000060000000L});

}