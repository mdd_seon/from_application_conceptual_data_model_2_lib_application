/**
 * generated by Xtext 2.21.0
 */
package br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.impl;

import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.Acm2LaPackage;
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.ManyToMany;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Many To Many</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ManyToManyImpl extends RelationImpl implements ManyToMany
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ManyToManyImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return Acm2LaPackage.Literals.MANY_TO_MANY;
  }

} //ManyToManyImpl
