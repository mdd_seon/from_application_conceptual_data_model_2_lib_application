/*
 * generated by Xtext 2.21.0
 */
package br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class Acm2LaAntlrTokenFileProvider implements IAntlrTokenFileProvider {

	@Override
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
		return classLoader.getResourceAsStream("br/ufes/nemo/seon/generator/applicationconceptualdata/to/application/parser/antlr/internal/InternalAcm2La.tokens");
	}
}
