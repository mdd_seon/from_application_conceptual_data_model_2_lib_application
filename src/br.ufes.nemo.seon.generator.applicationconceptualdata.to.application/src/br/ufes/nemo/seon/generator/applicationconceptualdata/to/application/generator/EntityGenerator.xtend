package br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.generator

import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.Attribute
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.Configuration
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.Entity
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.Function
import com.google.inject.Inject
import java.util.ArrayList
import java.util.List
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import org.eclipse.xtext.naming.IQualifiedNameProvider

class EntityGenerator extends AbstractGenerator {

	@Inject extension IQualifiedNameProvider

	var String lib_name;

	var PATH = "src/"
	
	var List<Entity> entities;

	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {

		for (configuration : resource.allContents.toIterable.filter(Configuration)) {

			lib_name = configuration.lib.name;
		}

		fsa.generateFile(PATH + lib_name + "/__init__.py", "")

		for (e : resource.allContents.toIterable.filter(Entity)) {
			fsa.generateFile(
				PATH + lib_name + "/" + e.fullyQualifiedName.toString("/").toLowerCase + ".py",
				e.compile
			)
		}
		
		entities = new ArrayList();

		entities.addAll(resource.allContents.toIterable.filter(Entity))
		
		fsa.generateFile(PATH + lib_name + "/" + "factories.py", createFactory(entities))

	}
	
	def createFactory(List<Entity> entities) '''
		import factory
		«FOR e: entities»
		from .«e.fullyQualifiedName.toLowerCase» import «e.fullyQualifiedName»
		«ENDFOR»
		
		«FOR e: entities»
		class «e.fullyQualifiedName»Factory(factory.Factory):
			class Meta:
				model = «e.fullyQualifiedName»
				  
		«ENDFOR»		

	'''

	def compile(Entity e) '''
		import logging
		logging.basicConfig(level=logging.INFO)
		
		«IF e.description !== null »«IF !e.description.textfield.isNullOrEmpty»""" «e.description.textfield» """«ENDIF»«ENDIF»
		class «e.name»«IF e.superType !== null»(«e.superType.fullyQualifiedName»)«ELSE»()«ENDIF»:
			def __init__(self):
				«IF e.superType !== null»
					super(«e.name»,self).__init__()
				«ENDIF»
				«FOR a : e.attributes»
					«a.compile»
				«ENDFOR»
				
			«FOR f : e.functions»
				«f.compile»
			«ENDFOR»
	'''

	def compile(Attribute a) '''
	
	«IF a.description !== null»«IF !a.description.textfield.isNullOrEmpty»""" «a.description.textfield» """«ENDIF»«ENDIF»
	self.«a.name» = None
	'''

	def compile(Function o) '''
	
	«IF o.description !== null »«IF !o.description.textfield.isNullOrEmpty»""" «o.description.textfield» """«ENDIF»«ENDIF»
	def «o.name»(self«FOR f : o.params»,«f»«ENDFOR»): 
		try:
			logging.info("Start function: «o.name»")
			#TO DO
			raise Exception('TO DO', 'TO DO')			
			logging.info("End function: «o.name»")
		except Exception as e: 
			logging.error("OS error: {0}".format(e))
			logging.error(e.__dict__) 
	'''

}
