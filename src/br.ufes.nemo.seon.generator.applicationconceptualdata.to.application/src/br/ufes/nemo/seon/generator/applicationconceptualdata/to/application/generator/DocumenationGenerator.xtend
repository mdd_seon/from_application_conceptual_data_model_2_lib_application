package br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.generator

import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.Entity
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.ManyToMany
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.OneToMany
import br.ufes.nemo.seon.generator.applicationconceptualdata.to.application.acm2La.OneToOne
import com.google.inject.Inject
import java.util.ArrayList
import java.util.List
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import org.eclipse.xtext.naming.IQualifiedNameProvider

class DocumenationGenerator extends AbstractGenerator {
	
	@Inject extension IQualifiedNameProvider

	var List<Entity> entities
	var PATH = "src/docs/"
	var GITLAB_PATH = "https://gitlab.com/mdd_seon/from_application_conceptual_data_model_2_lib_application"

	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {

		entities = new ArrayList();

		entities.addAll(resource.allContents.toIterable.filter(Entity))

		fsa.generateFile(PATH + "README.md", createDocumentation(entities))

		fsa.generateFile(PATH + "classdiagram.puml", createClassDiagram(entities))

	}

	def createDocumentation(List<Entity> entities) '''
		«IF entities.size()>0»
			# Documentation
			
			## Application Conceptual Data Model
			![Domain Diagram](classdiagram.png)	
				
			## Entities
				
			«FOR x: entities»
				«IF x.description !== null»
				 * **«x.name»** : «x.description.textfield»
				«ENDIF»
			«ENDFOR»
			
			«FOR x: entities»
				«IF x.functions !== null && x.functions.size()>0»
				## Functions of «x.name»:	
				«FOR f: x.functions»
				«var count = 0»
				«IF f.description !== null»* **«f.name» («FOR p: f.params»«count = count+1»«p»«IF count < f.params.size »,«ENDIF»«ENDFOR»)**: «f.description.textfield»
				«ELSE»* **«f.name» («FOR p: f.params»«p» , «ENDFOR»)**: - 
				«ENDIF»
				«ENDFOR»
				«ENDIF»
			«ENDFOR»
		«ENDIF»
				
		## Copyright
		This lib was PowerRight by [SEON Application Lib Generator](«GITLAB_PATH») 
	'''

	def createClassDiagram(List<Entity> entities) '''
		@startuml
		«IF entities.size()>0»
			«FOR e: entities»
				class «e.name»{
					«FOR a: e.attributes»
						«a.type»:«a.name»
					«ENDFOR»
					«FOR f: e.functions»
						«var count = 0»
						«f.type»: «f.name»(«FOR p: f.params»«p»«count = count+1»«IF count < f.params.size »,«ENDIF»«ENDFOR»)
					«ENDFOR»
				}
				«IF e.superType !== null»«e.superType.fullyQualifiedName» <|-- «e.name»«ENDIF»
				
				«FOR r: e.relations»
					«IF r.eClass.instanceClass == OneToOne»
						«e.name» "1" -- "1" «r.type.fullyQualifiedName» : «r.name» >
					«ENDIF»
					«IF r.eClass.instanceClass == ManyToMany»
						«e.name» "0..*" -- "0..*" «r.type.fullyQualifiedName» : «r.name» >
					«ENDIF»
					«IF r.eClass.instanceClass == OneToMany»
						«e.name» "1" -- "0..*" «r.type.fullyQualifiedName» : «r.name» >
					«ENDIF»
				«ENDFOR»
				
			«ENDFOR»
		«ENDIF»
		
		@enduml
	'''

	
}